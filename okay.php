<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Future UI - Bootstrap 3 Skin</title>
      <!-- #CSS Links -->
	  <!-- Basic Styles -->
      <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css"/>
      <link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>
      <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"/>
      <link href="assets/plugins/iCheck-master/skins/all.css?v=1.0.2" rel="stylesheet">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	  
	  <!-- #GOOGLE FONT -->
      <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>

          
	  
      
   </head>
	
 <!-- #BODY -->
   <body>
      <div id="wrapper">
         <div class="container">
   
            <!-- #NAVBAR -->
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Scoutingbadges.nl</h1>
                     </div>
                     <div class="bs-example">
                        <nav role="navigation" class="navbar navbar-default navbar-success">
                           <div class="container-fluid">
                              <!-- Brand and toggle get grouped for better mobile display -->
                              <div class="navbar-header">
                                 <button data-target="#bs-example-navbar-collapse-3" data-toggle="collapse" class="navbar-toggle" type="button">
                                 <span class="sr-only">Toggle navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 </button>
                                 <a href="http://www.NAAMSITE.nl" class="navbar-brand">Home</a>
                              </div>
                              <!-- Collect the nav links, forms, and other content for toggling -->
                              <div id="bs-example-navbar-collapse-3" class="collapse navbar-collapse">
                                 <ul class="nav navbar-nav">
                                    <li><a href="#">Over ons</a></li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">Download <b class="caret"></b></a>
                                       <ul class="dropdown-menu">
                                          <li><a href="http://www.NAAMSITE.nl">MENU1</a></li>

                                          <li class="divider"></li>
                                          <li><a href="http://www.NAAMSITE.nl/">LOREM</a></li>

                                       </ul>
                                    </li>

                                    <li><a href="http://www.NAAMSITEl.nl/">MENU</a></li>
                                    <li><a href="http://www.NAAMSITEl.nl/">MENU</a></li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">Zoeken <b class="caret"></b></a>
                                       <ul style="min-width: 300px;" class="dropdown-menu">
                                          <li>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <form role="search" class="navbar-form navbar-left">
                                                      <div class="input-group">
                                                         <input type="text" placeholder="Zoek" class="form-control">
                                                         <span class="input-group-btn">
                                                         <button type="button" class="btn btn-primary">
                                                         Ga!</button>
                                                         </span>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </li>
                                 </ul>
                                 <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Facebook</a></li>                                   
                                 </ul>
                              </div>
                           </div>
                           <!-- /.navbar-collapse -->
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
            <!-- #NAVBAR  end -->


<!-- #Content -->

<div class="row">
   <div class="col-lg-12">

   		<h1>Thank You!</h1>
                     
   </div>
</div>


<!-- #Content end -->


   <!--#Footer -->
      <div>
         <hr>
         @2017 <a href="http://www.NAAMSITE.nl">NAAMSITE.nl</a> - Lorem <a href="http://www.NAAMSITE.nl">LOREM IPSUM</a> en lorem 
         | <a href="http://www.NAAMSITE.nl/partners">Partners</a> 
         | <a href="http://www.NAAMSITE.nl/contact">Contact</a> 
         | <a href="http://www.NAAMSITE.nl/disclaimer">Disclaimer</a> - Lorem ipsum.
      </div>

<!--#Footer end -->

      </div>
   </div>
         
 
     
     <!-- #JS LINKS -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="http://www.NAAMSITE.nl/assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="http://www.NAAMSITE.nl/assets/plugins/iCheck-master/js/icheck.js?v=1.0.2"></script>
      <script>
         $(document).ready(function(){
           $('input').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
           });
         });
      </script>
      <script src="http://www.NAAMSITE.nl/assets/plugins/iCheck-master/js/icheck.js?v=1.0.2"></script>

      <!-- Connect jQuery -->
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>  
      <!-- Connect validate script plugin -->
      <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>   
   
      <!-- Connect custom script -->
      <script type="text/javascript" src="assets/js/custom.js"></script>        


   <!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=11204859; 
var sc_invisible=1; 
var sc_security="4f4d9de7"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script><script type="text/javascript" src="https://secure.statcounter.com/counter/counter.js"></script>
<noscript>&lt;div class="statcounter"&gt;&lt;a title="free hit
counter" href="http://statcounter.com/" target="_blank"&gt;&lt;img
class="statcounter"
src="//c.statcounter.com/11204859/0/4f4d9de7/1/" alt="free
hit counter"&gt;&lt;/a&gt;&lt;/div&gt;</noscript>
<!-- End of StatCounter Code for Default Guide -->      
   
</body>
</html>
