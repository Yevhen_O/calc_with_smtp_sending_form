// start ready block
$( document ).ready(function() {

	$('.measurement input').on('blur', function(){
		// switch class
		val = $(this).val();
		$(this).next('.unit').removeClass('c_1').removeClass('c_2').removeClass('c_3').removeClass('c_4').removeClass('c_5').removeClass('c_6').removeClass('c_7');	
		switch (true) {
		    case (val > 999999):
		        unit_class = 'c_7';
		        break;
		    case (val > 99999 && val < 1000000):
		        unit_class = 'c_6';
		        break;
		    case (val > 9999 && val < 100000):
		        unit_class = 'c_5';
		        break;
		    case (val > 999 && val < 10000):
		        unit_class = 'c_4';
		        break;
		    case (val > 99 && val < 1000):
		        unit_class = 'c_3';
		        break;
		    case (val > 9 && val < 100):
		        unit_class = 'c_2';
		        break;
		    default:
		        unit_class = 'c_1';
		        break;
		}	
		$(this).next('.unit').addClass(unit_class);	
	});

	errorClass = 'error';
	validClass = 'success';
	focusClass = 'highlight';

	if($('.webform').length) {
		$('.webform').each(function(){
			calculate_form($(this));
			$(this).validate({
				debug: true,
				//onkeyup: function(element) {$(element).valid()},
				errorElement: "span",
				errorPlacement: function(error, element) {
					error.appendTo(element.parent());
				},
				success: function(label, element) {
					label.text('');
				},
				rules: {
	                aantal:{
	                    required: true,
	                    min: 1
	                },
	                formaat:{
	                    required: true,
	                    min: 1,
	                    max: 100
	                },
	                naam: {
	                    required: true,
	                    minlength: 2
	                },
	                mail: {
	                	required: true,
	                    customMail: true
	                },
	                offerte:{
	                	required: function(element){return $(element).parents('form').find("input[name='bestellen']").is(":checked")!="1"; }
	                },
	                bestellen:{
	                	//required: function(element){return $(element).parents('form').find("input[name='offerte']").is(":checked")!="1"; }
	                }
	            },				
				messages: {
					aantal:{
	                    required: 'vul svp een waarde in',
	                    min: 'Voer een waarde in die groter is dan of gelijk is aan 1.'
	                },
	                formaat:{
	                    required: 'vul svp een waarde in',
	                    min: 'Voer een waarde in die groter is dan of gelijk is aan 1.',
	                    max: 'Voer een waarde in die kleiner is dan of gelijk is aan 100.'
	                },
	                naam: {
	                    required: 'vul svp een waarde in',
	                    minlength: 'Voer alstublieft minimaal 2 karakters in.'
	                },
	                mail: {
	                	required: 'vul svp een waarde in',
	                    customMail: 'Voer een geldig e-mailadres in.',
	                    email: 'Voer een geldig e-mailadres in.'
	                },
	                offerte:{
	                	required: 'vul svp een waarde in'
	                },
	                bestellen:{
	                	required: 'vul svp een waarde in'
	                }
				},
				submitHandler: function(form) {
					var $form    = form,
        			formData = new FormData(form);
        
					$.ajax({
					    type: "POST",
					    url: "assets/plugins/calculation_form/send_form.php",
					    dataType: "json",
					    data: formData,
					    mimeType: "multipart/form-data",
					    contentType: false,
					    processData: false
					}).done(function (response) {
					    if (response.status != 'success') {
					        alert("Failed");
					        console.log(response.message);
					    }
					    else {
					        //alert("Thank You!");
					        window.location.href = '/dev/okay.php'; 
					    }
					});
		        }
			});	 // end validate
		}) // end each

		$('.webform select, .webform input').change(function(){
			calculate_form($(this).parents('.webform'));
		});

	} // end if webform



});
// end ready block 

// counting rules
var rules = {
	amount:{
		25:{
			format:{
				50:{
					price:2.18,
					price_b:3.23,
					price_d:1.96,
					price_bd:2.9,
				},
				75:{
					price:4.1,
					price_b:7.47,
					price_d:3.69,
					price_bd:6.72,
				},
				90:{
					price:5.25,
					price_b:8.48,
					price_d:4.72,
					price_bd:7.62,
				},
				100:{
					price:6.12,
					price_b:10.46,
					price_d:5.5,
					price_bd:9.4,
				},
			},
		},
		50:{
			format:{
				50:{
					price:0.93,
					price_b:1.38,
					price_d:0.84,
					price_bd:1.24,
				},
				75:{
					price:1.75,
					price_b:3.2,
					price_d:1.58,
					price_bd:2.88,
				},
				90:{
					price:2.25,
					price_b:3.63,
					price_d:2.02,
					price_bd:3.27,
				},
				100:{
					price:2.62,
					price_b:4.68,
					price_d:2.35,
					price_bd:4.03,
				},
			},
		},
		100:{
			format:{
				50:{
					price:0.67,
					price_b:0.99,
					price_d:0.60,
					price_bd:0.89,
				},
				75:{
					price:1.17,
					price_b:2.01,
					price_d:1.05,
					price_bd:1.81,
				},
				90:{
					price:1.45,
					price_b:2.44,
					price_d:1.3,
					price_bd:2.2,
				},
				100:{
					price:1.75,
					price_b:3.01,
					price_d:1.58,
					price_bd:2.7,
				},
			},
		},
		250:{
			format:{
				50:{
					price:0.52,
					price_b:0.78,
					price_d:0.47,
					price_bd:0.7,
				},
				75:{
					price:0.91,
					price_b:1.43,
					price_d:0.82,
					price_bd:1.28,
				},
				90:{
					price:1.08,
					price_b:1.8,
					price_d:0.97,
					price_bd:1.61,
				},
				100:{
					price:1.3,
					price_b:2.21,
					price_d:1.17,
					price_bd:1.98,
				},
			},
		},
		500:{
			format:{
				50:{
					price:0.39,
					price_b:0.58,
					price_d:0.35,
					price_bd:0.53,
				},
				75:{
					price:0.65,
					price_b:1.06,
					price_d:0.58,
					price_bd:0.95,
				},
				90:{
					price:0.82,
					price_b:1.28,
					price_d:0.74,
					price_bd:1.15,
				},
				100:{
					price:0.97,
					price_b:1.62,
					price_d:0.88,
					price_bd:1.46,
				},
			},
		},
		1000:{
			format:{
				50:{
					price:0.32,
					price_b:0.48,
					price_d:0.29,
					price_bd:0.43,
				},
				75:{
					price:0.58,
					price_b:0.89,
					price_d:0.53,
					price_bd:0.8,
				},
				90:{
					price:0.69,
					price_b:1.06,
					price_d:0.62,
					price_bd:0.95,
				},
				100:{
					price:0.89,
					price_b:1.32,
					price_d:0.8,
					price_bd:1.19,
				},
			},
		},
		2000:{
			format:{
				50:{
					price:0.28,
					price_b:0.43,
					price_d:0.25,
					price_bd:0.39,
				},
				75:{
					price:0.52,
					price_b:0.82,
					price_d:0.47,
					price_bd:0.74,
				},
				90:{
					price:0.65,
					price_b:0.95,
					price_d:0.58,
					price_bd:0.86,
				},
				100:{
					price:0.78,
					price_b:1.17,
					price_d:0.7,
					price_bd:1.05,
				},
			},
		},
	},
	backside:{
		standart: 0,
		opstrijkbaar: 0.1,
		zelfklevend: 0.1,
		klittenband: 0.4,
	},
	discount_code:'AA1354CD',
	shipping: 6.5,
};

	function calculate_form(form){
		amount = form.find('input[name="aantal"]').val();
		format = form.find('input[name="formaat"]').val();
		borduring = form.find('select[name="borduring"]').val();
		backside = form.find('select[name="achterzijde"]').val();
		kortingscode = form.find('input[name="kortingscode"]').val();

		if(kortingscode == rules.discount_code){
			price_label = (borduring != 1)? 'price_d':'price_bd';
		} else {
			price_label = (borduring != 1)? 'price':'price_b';
		}

		// switch ammount steep
		switch (true) {
		    case (amount >= 2000):
		        amount_label = '2000';
		        break;
		    case (amount >= 1000 && amount < 2000):
		        amount_label = '1000';
		        break;
		    case (amount >= 500 && amount < 1000):
		        amount_label = '500';
		        break;
		    case (amount >= 250 && amount < 500):
		        amount_label = '250';
		        break;
		    case (amount >= 100 && amount < 250):
		        amount_label = '100';
		        break;
		    case (amount >= 50 && amount < 100):
		        amount_label = '50';
		        break;
		    default:
		        amount_label = '25';
		        break;
		}
		// switch format steep
		switch (true) {
		    case (format <= 50):
		        format_label = '50';
		        break;
		    case (format > 50 && format <= 75):
		        format_label = '75';
		        break;
		    case (format > 75 && format <= 90):
		        format_label = '90';
		        break;
		    default:
		        format_label = '100';
		        break;
		}

		// Get current price
		price = rules.amount[amount_label].format[format_label][price_label];

		// Check backside
		switch (true) {
		    case (backside == 'Klittenband'):
		        price_bs = rules.backside.klittenband;
		        break;
		    case (backside == 'Opstrijkbaar'):
		        price_bs = rules.backside.opstrijkbaar;
		        break;
		    case (backside == 'Zelfklevend'):
		        price_bs = rules.backside.zelfklevend;
		        break;
		    default:
		        price_bs = rules.backside.standart;
		        break;
		}	

		full_price = parseFloat(price)+parseFloat(price_bs);
		//full_price = full_price.toFixed(2);

		vat_price = full_price*1.21;
		//vat_price = vat_price.toFixed(2);

		amount_total = vat_price*amount;
		//amount_total = amount_total.toFixed(2);

		amount_ship = parseFloat(amount_total)+parseFloat(rules.shipping);

		p_st = amount_ship/amount;
		amount_ship = amount_ship.toFixed(2).toString().replace(/\./g, ',');
		p_st = p_st.toFixed(2).toString().replace(/\./g, ',');



		// debug = '<h3>Debug</h3> \n <br />\
		// Amount: '+amount+'\n<br/>\
		// Format: '+format+'\n<br/>\
		// Borduring: '+borduring+'\n<br/>\
		// Backside: '+backside+'\n<br/>\
		// Kortingscode: '+kortingscode+'\n<br/>\
		// price_label: '+price_label+'\n<br/>\
		// amount_label: '+amount_label+'\n<br/>\
		// format_label: '+format_label+'\n<br/>\
		// price: '+price+'\n<br/>\
		// price_bs: '+price_bs+'\n<br/>\
		// full_price: '+full_price+'\n<br/>\
		// vat_price: '+vat_price+'\n<br/>\
		// amount_total: '+amount_total+'\n<br/>\
		// amount_ship: '+amount_ship+'\n<br/>\
		// p_st: '+p_st+'\n<br/>\
		// ';

		result = '<div class="float-left bg_black">\
		<div><span class="large">€ '+amount_ship+'</span><span class=""> incl. BTW</span><div>\
		<div><span class="per_st">P.st. €'+p_st+'</span></div>\
		</div>';

		// $('#debug').html(debug);

		form.find('.per_item').val(p_st);
		form.find('.total_amount').val(amount_ship);
		form.find('.counting_result').html(result);
	}

$.validator.addMethod("customMail", function(value, element) { 
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value); 
}, "Please enter valid email address.");
