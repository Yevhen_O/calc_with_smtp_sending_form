<div class="form_wrapper clearfix">
	<form class="webform" method="post" action="/send_form.php" enctype='multipart/form-data'>
		<div class="counting_result clearfix"></div>
		<div class="form-group float-left">
		    <label for="aantal">Aantal</label>
		    <input name="aantal" type="number" class="form-control" id="aantal" placeholder="100" value="100" min="1">
		</div>
		<div class="row clear_b">
			<div class="col-xs-6 col-lg-3">
				<div class="form-group measurement">
				    <label for="formaat">Formaat</label>
				    <input name="formaat" type="number" class="form-control" id="formaat" placeholder="75" value="75" min="1">
				    <span class="unit">mm</span>
				</div>									
			</div>	
			<div class="col-xs-6 col-lg-3">
				<div class="form-group">
				    <label for="Borduring">Borduring</label>
				    <select name="borduring" class="form-control" id="Borduring">
				      <option value="0" checked>< 70%</option>
				      <option value="1">> 70%</option>
				    </select>
				</div>
			</div>
			<div class="col-xs-6 col-lg-3">
				<div class="form-group">
				    <label for="Achterzijde">Achterzijde</label>
				    <select name="achterzijde" class="form-control" id="Achterzijde">
				      <option value="Standaard" checked >Standaard</option>
				      <option value="Opstrijkbaar">Opstrijkbaar</option>
				      <option value="Zelfklevend">Zelfklevend</option>
				      <option value="Klittenband">Klittenband</option>
				    </select>
				</div>
			</div>
			<div class="col-xs-6 col-lg-3">
				<div class="form-group ">
				    <label for="uploaden">Bestand uploaden</label>
					<div class="form-control file">
						<input class="form-control" name="uploaden" id="uploaden" type="file" accept="image/gif,image/jpeg,image/png,application/pdf,application/postscript,image/vnd.adobe.photoshop">
					</div> 
				</div>								
				
				
			</div>
		</div> <!-- .rox.clear_b -->
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="form-group float-left">
					<label for="Kortingscode">Kortingscode </label>
				    <input name="kortingscode" type="text" class="form-control" id="Kortingscode" placeholder="" value="">	
					<hr>	
				</div>
			</div>
		</div>
		<div class="form-check">
		  <label class="custom-control custom-checkbox">
		    <input name="bestellen" id="bestellen" class="custom-control-input" type="checkbox" value="1">
		    <span class="custom-control-indicator"></span>
		    <span class="custom-control-description">Ik wil graag bestellen. Stuur mij eerst een drukproef ter goedkeuring.</span>
		  </label>
		</div>
		<div class="form-check">
		  <label class="custom-control custom-checkbox">
		    <input name="offerte" id="offerte" class="custom-control-input" type="checkbox" value="1">
		    <span class="custom-control-indicator"></span>
		    <spam class="custom-control-description">Ik wil graag eerst een offerte.</spam>
		  </label>
		</div>
		<br>
		<div class="row clear_b">
			<div class="col-xs-6 col-sm-6">
				<div class="form-group">
				    <label for="naam">Naam</label>
				    <input name="naam" type="text" class="form-control" id="naam" placeholder="" value="">
				</div>									
			</div>	
			<div class="col-xs-6 col-sm-6">
				<div class="form-group">
				    <label for="mail">E-mail</label>
				    <input name="mail" type="email" class="form-control" id="mail" placeholder="" value="">
				</div>
			</div>
		</div> <!-- .rox.clear_b -->			
		<br>
		<input type="hidden" class="total_amount" name="total_amount">
		<input type="hidden" class="per_item" name="per_item">
		<button type="submit" class="btn btn-primary">Verzenden</button>					
	</form>
</div> <!-- .form_wrapper -->
