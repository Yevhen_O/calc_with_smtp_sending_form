<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Future UI - Bootstrap 3 Skin</title>
      <!-- #CSS Links -->
	  <!-- Basic Styles -->
      <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css"/>
      <link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>
      <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.css"/>
      <link href="assets/plugins/iCheck-master/skins/all.css?v=1.0.2" rel="stylesheet">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	  
	  <!-- #GOOGLE FONT -->
      <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	  
      
   </head>
   
   <!--

	TABLE OF CONTENTS.
	
	Use search to find needed section.
	
	===================================================================
	
	|  01. #CSS Links                |  all CSS links and file paths  |
	|  02. #GOOGLE FONT              |  Google font link              |
	|  03. #BODY                     |  body tag                      |
	|  04. #THEMETOP                 |  Theme top Secton              |
	|  05. #NAVBAR                   |  Menu navigation               |
	|  06. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
	|  07. #Typography               |  Typography                    |
	|  08. #Modal toolip popover     |  Modal toolip popover          |
	|  09. #Forms Elements           |  Forms Elements                |
	|  10. #Buttons styles           |  Different Buttons style       |
	|  11. #NAVS                     |  Tabs accordion pills          |
	|  12. #INDICATORS               |  2 Different indicators        |
	|  13. #TABLES                   |  Different tables              |
	|  14. #CONTAINERS               |  Containers for elements       |
	|  15. #GALLERY ITEMS            |  Sliders and images            |
	|  16. #JS Links                 |  all scripts and plugins       |
	
	===================================================================
	
	-->
	
 <!-- #BODY -->
   <body>
      <div id="wrapper">
         <div class="container">
            <!-- #THEMETOP -->
            <div class="row">
               <div class="col-lg-12">
                  <div class="theme-logo">
                     <div class="theme-text">
                        <i class="fa fa-lightbulb-o fa-3x"></i>
                        <h1>Future UI</h1>
                        <h4>Built with Bootstrap 3</h4>
                     </div>
                     <div class="row">
                        <div class="col-md-12 text-center"><a class="btn btn-success btn-lg" href="index.html">Bootstrap Elements</a>
                           <a class="btn btn-success btn-lg" href="other-elements.html">Other Elements</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #NAVBAR -->
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Navbar</h1>
                     </div>
                     <div class="bs-example">
                        <div role="navigation" class="navbar navbar-default">
                           <div class="container-fluid">
                              <div class="navbar-header">
                                 <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                                 <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                 </button>
                                 <a href="#" class="navbar-brand">Company Logo</a>
                              </div>
                              <!-- Collect the nav links, forms, and other content for toggling -->
                              <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                                 <ul class="nav navbar-nav">
                                    <li class="active"><a href="#"><span class="fa fa-home"></span>Dashboard</a></li>
                                    <li><a href="#"><span class="fa fa-calendar"></span>About Us</a></li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-list-alt"></span>Pages <b class="caret"></b></a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">One more separated link</a></li>
                                       </ul>
                                    </li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-search"></span>Search <b class="caret"></b></a>
                                       <ul style="min-width: 300px;" class="dropdown-menu">
                                          <li>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <form role="search" class="navbar-form navbar-left">
                                                      <div class="input-group">
                                                         <input type="text" placeholder="Search" class="form-control">
                                                         <span class="input-group-btn">
                                                         <button type="button" class="btn btn-primary">
                                                         Go!</button>
                                                         </span>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </li>
                                 </ul>
                                 <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-comment"></span>Messages <span class="label label-primary">4</span>
                                       </a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#"><span class="label label-danger">5:00 AM</span>Hi :)</a></li>
                                          <li><a href="#"><span class="label label-danger">5:50 AM</span>Are you Der?</a></li>
                                          <li><a href="#"><span class="label label-danger">6:00 AM</span>Please Help Me?</a></li>
                                          <li class="divider"></li>
                                          <li><a class="text-center" href="#">View All</a></li>
                                       </ul>
                                    </li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-envelope"></span>Inbox <span class="label label-info">101</span>
                                       </a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#"><span class="label label-danger">4:00 AM</span>Lorem ipsum dolor</a></li>
                                          <li><a href="#"><span class="label label-danger">4:00 AM</span>Lorem ipsum </a></li>
                                          <li><a href="#"><span class="label label-danger">4:00 AM</span>Lorem ipsum design</a>
                                          </li>
                                          <li class="divider"></li>
                                          <li><a class="text-center" href="#">View All</a></li>
                                       </ul>
                                    </li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fa fa-user"></span>Admin <b class="caret"></b></a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#"><span class="fa fa-user"></span>Profile</a></li>
                                          <li><a href="#"><span class="fa fa-cog"></span>Settings</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#"><span class="fa fa-off"></span>Logout</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <!-- /.navbar-collapse -->
                        </div>
                     </div>
                     <div class="bs-example">
                        <nav class="navbar  navbar-inverse" role="navigation">
                           <div class="container-fluid">
                              <!-- Brand and toggle get grouped for better mobile display -->
                              <div class="navbar-header">
                                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                 <span class="sr-only">Toggle navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 </button>
                                 <a class="navbar-brand" href="#">Company Logo</a>
                              </div>
                              <!-- Collect the nav links, forms, and other content for toggling -->
                              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                 <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Link</a></li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                       <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">One more separated link</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                                 <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                       <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                              <!-- /.navbar-collapse -->
                           </div>
                           <!-- /.container-fluid -->
                        </nav>
                     </div>
                     <div class="bs-example">
                        <nav role="navigation" class="navbar navbar-default navbar-success">
                           <div class="container-fluid">
                              <!-- Brand and toggle get grouped for better mobile display -->
                              <div class="navbar-header">
                                 <button data-target="#bs-example-navbar-collapse-3" data-toggle="collapse" class="navbar-toggle" type="button">
                                 <span class="sr-only">Toggle navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 </button>
                                 <a href="#" class="navbar-brand">Company Logo</a>
                              </div>
                              <!-- Collect the nav links, forms, and other content for toggling -->
                              <div id="bs-example-navbar-collapse-3" class="collapse navbar-collapse">
                                 <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Home</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">Pages <b class="caret"></b></a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">One more separated link</a></li>
                                       </ul>
                                    </li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">Search <b class="caret"></b></a>
                                       <ul style="min-width: 300px;" class="dropdown-menu">
                                          <li>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <form role="search" class="navbar-form navbar-left">
                                                      <div class="input-group">
                                                         <input type="text" placeholder="Search" class="form-control">
                                                         <span class="input-group-btn">
                                                         <button type="button" class="btn btn-primary">
                                                         Go!</button>
                                                         </span>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </li>
                                       </ul>
                                    </li>
                                 </ul>
                                 <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Sign Up</a></li>
                                    <li class="dropdown">
                                       <a data-toggle="dropdown" class="dropdown-toggle" href="#">Sign in <b class="caret"></b></a>
                                       <ul style="padding: 15px;min-width: 250px;" class="dropdown-menu">
                                          <li>
                                             <div class="row">
                                                <div class="col-md-12">
                                                   <form id="login-nav" accept-charset="UTF-8" action="login" method="post" role="form" class="form">
                                                      <div class="form-group">
                                                         <label for="exampleInputEmail2" class="sr-only">Email address</label>
                                                         <input type="email" required="" placeholder="Email address" id="exampleInputEmail2" class="form-control">
                                                      </div>
                                                      <div class="form-group">
                                                         <label for="exampleInputPassword2" class="sr-only">Password</label>
                                                         <input type="password" required="" placeholder="Password" id="exampleInputPassword2" class="form-control">
                                                      </div>
                                                      <div class="checkbox">
                                                         <label>
                                                         <input type="checkbox"> Remember me
                                                         </label>
                                                      </div>
                                                      <div class="form-group">
                                                         <button class="btn btn-success btn-block" type="submit">Sign in</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="divider"></li>
                                          <li>
                                             <input type="button" value="Sign In with Google" id="sign-in-google" class="btn btn-primary btn-block">
                                             <input type="button" value="Sign In with Twitter" id="sign-in-twitter" class="btn btn-primary btn-block">
                                          </li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <!-- /.navbar-collapse -->
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #TOGGLE LAYOUT BUTTONS -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Buttons</h1>
                     </div>
                     <div class="bs-example">
                        <div class="row">
                           <div class="col-lg-8">
                              <div class="bs-example">
                                 <p>
                                    <button class="btn btn-lg btn-default" type="button">Default</button>
                                    <button class="btn btn-lg btn-primary" type="button">Primary</button>
                                    <button class="btn btn-lg btn-success" type="button">Success</button>
                                    <button class="btn btn-lg btn-info" type="button">Info</button>
                                    <button class="btn btn-lg btn-warning" type="button">Warning</button>
                                    <button class="btn btn-lg btn-danger" type="button">Danger</button>
                                    <button class="btn btn-lg btn-anchor" type="button">Link</button>
                                 </p>
                              </div>
                              <div class="bs-example">
                                 <p>
                                    <button type="button" class="btn btn-default">Default</button>
                                    <button type="button" class="btn btn-primary">Primary</button>
                                    <button type="button" class="btn btn-success">Success</button>
                                    <button type="button" class="btn btn-info">Info</button>
                                    <button type="button" class="btn btn-warning">Warning</button>
                                    <button type="button" class="btn btn-danger">Danger</button>
                                    <button type="button" class="btn btn-anchor">Link</button>
                                 </p>
                              </div>
                              <div class="bs-example">
                                 <p>
                                    <button type="button" class="btn btn-default disabled">Default</button>
                                    <button type="button" class="btn btn-primary disabled">Primary</button>
                                    <button type="button" class="btn btn-success disabled">Success</button>
                                    <button type="button" class="btn btn-info disabled">Info</button>
                                    <button type="button" class="btn btn-warning disabled">Warning</button>
                                    <button type="button" class="btn btn-danger disabled">Danger</button>
                                    <button type="button" class="btn btn-anchor disabled">Link</button>
                                 </p>
                              </div>
                              <div class="bs-example">
                                 <p>
                                    <button class="btn btn-sm btn-default" type="button">Default</button>
                                    <button class="btn btn-sm btn-primary" type="button">Primary</button>
                                    <button class="btn btn-sm btn-success" type="button">Success</button>
                                    <button class="btn btn-sm btn-info" type="button">Info</button>
                                    <button class="btn btn-sm btn-warning" type="button">Warning</button>
                                    <button class="btn btn-sm btn-danger" type="button">Danger</button>
                                    <button class="btn btn-sm btn-anchor" type="button">Link</button>
                                 </p>
                              </div>
                              <div class="bs-example">
                                 <p>
                                    <button class="btn btn-xs btn-default" type="button">Default</button>
                                    <button class="btn btn-xs btn-primary" type="button">Primary</button>
                                    <button class="btn btn-xs btn-success" type="button">Success</button>
                                    <button class="btn btn-xs btn-info" type="button">Info</button>
                                    <button class="btn btn-xs btn-warning" type="button">Warning</button>
                                    <button class="btn btn-xs btn-danger" type="button">Danger</button>
                                    <button class="btn btn-xs btn-anchor" type="button">Link</button>
                                 </p>
                              </div>
                              <div class="bs-example" style="margin-bottom: 15px;">
                                 <div class="btn-toolbar" style="margin: 0;">
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-default">Default</button>
                                       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-primary">Primary</button>
                                       <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-success">Success</button>
                                       <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-info">Info</button>
                                       <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-warning">Warning</button>
                                       <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </div>
                                    <!-- /btn-group -->
                                 </div>
                                 <!-- /btn-toolbar -->
                              </div>
                              <div class="bs-example">
                                 <p>
                                    <button type="button" class="btn btn-primary btn-lg">Large button</button>
                                    <button type="button" class="btn btn-primary">Default button</button>
                                    <button type="button" class="btn btn-primary btn-sm">Small button</button>
                                    <button type="button" class="btn btn-primary btn-xs">Mini button</button>
                                 </p>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <p>
                                    <button type="button" class="btn btn-default btn-lg btn-block">Block level button</button>
                                 </p>
                              </div>
                              <div class="bs-example" style="margin-bottom: 15px;">
                                 <div class="btn-group btn-group-justified">
                                    <a href="#" class="btn btn-default">Left</a>
                                    <a href="#" class="btn btn-default">Middle</a>
                                    <a href="#" class="btn btn-default">Right</a>
                                 </div>
                              </div>
                              <div class="bs-example" style="margin-bottom: 15px;">
                                 <div class="btn-toolbar">
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-default">1</button>
                                       <button type="button" class="btn btn-default">2</button>
                                       <button type="button" class="btn btn-default">3</button>
                                       <button type="button" class="btn btn-default">4</button>
                                    </div>
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-default">5</button>
                                       <button type="button" class="btn btn-default">6</button>
                                       <button type="button" class="btn btn-default">7</button>
                                    </div>
                                    <div class="btn-group">
                                       <button type="button" class="btn btn-default">8</button>
                                       <div class="btn-group">
                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                          Dropdown
                                          <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu">
                                             <li><a href="#">Dropdown link</a></li>
                                             <li><a href="#">Dropdown link</a></li>
                                             <li><a href="#">Dropdown link</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="btn-group-vertical">
                                    <button type="button" class="btn btn-default">Button</button>
                                    <button type="button" class="btn btn-default">Button</button>
                                    <button type="button" class="btn btn-default">Button</button>
                                    <button type="button" class="btn btn-default">Button</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #Typography -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Typography</h1>
                     </div>
                     <div class="bs-group">
                        <div class="row">
                           <div class="col-lg-4">
                              <div class="bs-example bs-example-type">
                                 <h1>Heading 1</h1>
                                 <h2>Heading 2</h2>
                                 <h3>Heading 3</h3>
                                 <h4>Heading 4</h4>
                                 <h5>Heading 5</h5>
                                 <h6>Heading 6</h6>
                              </div>
                              <div class="bs-example">
                                 <p class="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <h2>Example body text</h2>
                                 <p>Nullam quis risus eget <a href="#">urna mollis ornare</a> vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                                 <p><small>This line of text is meant to be treated as fine print.</small></p>
                                 <p>The following snippet of text is <strong>rendered as bold text</strong>.</p>
                                 <p>The following snippet of text is <em>rendered as italicized text</em>.</p>
                                 <p>An abbreviation of the word attribute is <abbr title="attribute">attr</abbr>.</p>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2>Emphasis classes</h2>
                              <div class="bs-example">
                                 <p class="text-muted">Fusce dapibus, tellus ac cursus commodo, tortor mauris nibh.</p>
                                 <p class="text-primary">Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                 <p class="text-warning">Etiam porta sem malesuada magna mollis euismod.</p>
                                 <p class="text-danger">Donec ullamcorper nulla non metus auctor fringilla.</p>
                                 <p class="text-success">Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>
                                 <p class="text-info">Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 id="type-blockquotes">Blockquotes</h2>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="bs-example">
                              <blockquote>
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                 <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                              </blockquote>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="bs-example">
                              <blockquote class="pull-right">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                 <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                              </blockquote>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #Modal toolip popover -->
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Dialogs</h1>
                     </div>
                     <div class="bs-docs-section">
                        <div class="row">
                           <div class="col-md-4">
                              <h2>Modals</h2>
                              <!-- Button trigger modal -->
                              <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                              Launch demo modal
                              </button>
                              <br><br>
                              <!-- Modal -->
                              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                       </div>
                                       <div class="modal-body">
                                          ...
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Large modal -->
                              <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
                              <br><br>
                              <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
                                 <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                          <h4 class="modal-title" id="myModalLabel2">Modal title</h4>
                                       </div>
                                       <div class="modal-body">
                                          ...
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Small modal -->
                              <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>
                              <br><br>
                              <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                 <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                          <h4 class="modal-title" id="myModalLabel3">Modal title</h4>
                                       </div>
                                       <div class="modal-body">
                                          ...
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="button" class="btn btn-primary">Save changes</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8">
                              <h2>Tooltip</h2>
                              <div class="bs-example">
                                 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="Tooltip on left">Tooltip on left</button>
                                 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Tooltip on top</button>
                                 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Tooltip on bottom</button>
                                 <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Tooltip on right</button>
                              </div>
                              <br>
                              <h2>Popover</h2>
                              <div class="bs-example">
                                 <button type="button" class="btn btn-success" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                 Popover on left
                                 </button>
                                 <button type="button" class="btn btn-success" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                 Popover on top
                                 </button>
                                 <button type="button" class="btn btn-success" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus
                                    sagittis lacus vel augue laoreet rutrum faucibus.">
                                 Popover on bottom
                                 </button>
                                 <button type="button" class="btn btn-success" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                 Popover on right
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!--- #Forms Elements -->
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Forms</h1>
                     </div>
                     <div class="bs-example">
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="well">
                                 <form class="bs-example form-horizontal">
                                    <fieldset>
                                       <legend>Legend</legend>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label" for="inputEmail">Email</label>
                                          <div class="col-lg-10">
                                             <input type="text" placeholder="Email" id="inputEmail" class="form-control">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label" for="inputEmail">Email</label>
                                          <div class="col-lg-10">
                                             <div class="left-inner-icon ">
                                                <i class="fa fa-envelope"></i>
                                                <input type="text" class="form-control">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-md-2 control-label">Email</label>
                                          <div class="col-md-10">
                                             <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="email" placeholder="Email Address" class="form-control">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-md-2 control-label">Password</label>
                                          <div class="col-md-10">
                                             <div class="input-group">
                                                <input type="password" placeholder="Password" class="form-control">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label" for="inputPassword">Password</label>
                                          <div class="col-lg-10">
                                             <input type="password" placeholder="Password" id="inputPassword" class="form-control">
                                             <div class="checkbox">
                                                <label>
                                                <input type="checkbox"> Checkbox
                                                </label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label" for="textArea">Textarea</label>
                                          <div class="col-lg-10">
                                             <textarea id="textArea" rows="3" class="form-control"></textarea>
                                             <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label">Radios</label>
                                          <div class="col-lg-10">
                                             <div class="radio">
                                                <label>
                                                <input type="radio" checked="" value="option1" id="optionsRadios1" name="optionsRadios">
                                                Option one is this
                                                </label>
                                             </div>
                                             <div class="radio">
                                                <label>
                                                <input type="radio" value="option2" id="optionsRadios2" name="optionsRadios">
                                                Option two can be something else
                                                </label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-lg-2 control-label" for="select">Selects</label>
                                          <div class="col-lg-10">
                                             <select id="select" class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                             </select>
                                             <br>
                                             <select class="form-control" multiple="">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-lg-10 col-lg-offset-2">
                                             <button class="btn btn-default">Cancel</button> 
                                             <button class="btn btn-primary" type="submit">Submit</button> 
                                          </div>
                                       </div>
                                    </fieldset>
                                 </form>
                              </div>
                           </div>
                           <div class="col-lg-4 col-lg-offset-1">
                              <form class="bs-example">
                                 <div class="form-group">
                                    <label for="focusedInput" class="control-label">Focused input</label>
                                    <input type="text" value="This is focused..." id="focusedInput" class="form-control" />
                                 </div>
                                 <div class="form-group">
                                    <label for="disabledInput" class="control-label">Disabled input</label>
                                    <input type="text" disabled="" placeholder="Disabled input here..." id="disabledInput" class="form-control" />
                                 </div>
                                 <div class="form-group has-warning">
                                    <label for="inputWarning" class="control-label">Input warning</label>
                                    <input type="text" id="inputWarning" class="form-control" />
                                 </div>
                                 <div class="form-group has-warning">
                                    <label for="inputWarning1" class="control-label">Input warning</label>
                                    <div class="right-inner-icon">
                                       <i class="fa fa-warning"></i>
                                       <input type="text" id="inputWarning1" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="form-group has-error">
                                    <label for="inputError" class="control-label">Input error</label>
                                    <input type="text" id="inputError" class="form-control" />
                                 </div>
                                 <div class="form-group has-error">
                                    <label for="inputError2" class="control-label">Input error</label>
                                    <div class="right-inner-icon">
                                       <i class="fa fa-times-circle"></i>
                                       <input type="text" id="inputError2" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="form-group has-success">
                                    <label for="inputSuccess" class="control-label">Input success</label>
                                    <input type="text" id="inputSuccess" class="form-control" />
                                 </div>
                                 <div class="form-group has-success">
                                    <label for="inputSuccess2" class="control-label">Input success</label>
                                    <div class="right-inner-icon">
                                       <i class="fa fa-check-circle"></i>
                                       <input type="text" id="inputSuccess2" class="form-control" />
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="inputLarge" class="control-label">Large input</label>
                                    <input type="text" id="inputLarge" class="form-control input-lg" />
                                 </div>
                                 <div class="form-group">
                                    <label for="inputDefault" class="control-label">Default input</label>
                                    <input type="text" id="inputDefault" class="form-control" />
                                 </div>
                                 <div class="form-group">
                                    <label for="inputSmall" class="control-label">Small input</label>
                                    <input type="text" id="inputSmall" class="form-control input-sm" />
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Input addons</label>
                                    <div class="input-group">
                                       <span class="input-group-addon">$</span>
                                       <input type="text" class="form-control" />
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-default">Button</button>
                                       </span>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #Buttons styles  -->		
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Cool Buttons With Labels </h1>
                     </div>
                     <div class="bs-example-component">
                        <div class="row">
                           <div class="col-md-4">
                              <div class="bs-example">
                                 <p><button class="btn btn-labeled btn-success" type="button">
                                    <span class="btn-label"><i class="fa fa-check"></i></span>Success</button>
                                    <button class="btn btn-labeled btn-danger" type="button">
                                    <span class="btn-label"><i class="fa fa-times"></i></span>Cancel</button>
                                 </p>
                                 <p><button class="btn btn-labeled btn-warning" type="button">
                                    <span class="btn-label"><i class="fa fa-bookmark"></i></span>Bookmark</button>
                                    <button class="btn btn-labeled btn-primary" type="button">
                                    <span class="btn-label"><i class="fa fa-camera"></i></span>Camera</button>
                                 </p>
                                 <p><button class="btn btn-labeled btn-default" type="button">
                                    <span class="btn-label"><i class="fa fa-chevron-left"></i></span>Left</button>
                                    <button class="btn btn-labeled btn-default" type="button">
                                    <span class="btn-label"><i class="fa fa-chevron-right"></i></span>
                                    Right</button>
                                 </p>
                                 <p><button class="btn btn-labeled btn-success" type="button">
                                    <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>Thumbs
                                    Up</button>
                                    <button class="btn btn-labeled btn-danger" type="button">
                                    <span class="btn-label"><i class="fa fa-thumbs-down"></i></span>Thumbs
                                    Down</button>
                                 </p>
                                 <p><button class="btn btn-labeled btn-info" type="button">
                                    <span class="btn-label"><i class="fa fa-refresh"></i></span>Refresh</button>
                                    <button class="btn btn-labeled btn-danger" type="button">
                                    <span class="btn-label"><i class="fa fa-trash-o"></i></span>Trash</button>
                                 </p>
                                 <a role="button" href="#" class="btn btn-success btn-labeled">
                                 <span class="btn-label"><i class="fa fa-info-sign"></i></span>Info Web</a>
                              </div>
                           </div>
                           <div class="col-md-8">
                              <h2>Small</h2>
                              <div class="bs-example">
                                 <button class="btn btn-default btn-circle" type="button"><i class="fa fa-check"></i></button>
                                 <button class="btn btn-primary btn-circle" type="button"><i class="fa fa-plus"></i></button>
                                 <button class="btn btn-success btn-circle" type="button"><i class="fa fa-link"></i></button>
                                 <button class="btn btn-info btn-circle" type="button"><i class="fa fa-pencil-square-o"></i></button>
                                 <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-times"></i></button>
                                 <button class="btn btn-danger btn-circle" type="button"><i class="fa  fa-paperclip"></i></button>
                              </div>
                              <h2>Larg</h2>
                              <div class="bs-example">
                                 <button class="btn btn-default btn-circle btn-lg" type="button"><i class="fa fa-check"></i></button>
                                 <button class="btn btn-primary btn-circle btn-lg" type="button"><i class="fa fa-plus"></i></button>
                                 <button class="btn btn-success btn-circle btn-lg" type="button"><i class="fa fa-link"></i></button>
                                 <button class="btn btn-info btn-circle btn-lg" type="button"><i class="fa fa-pencil-square-o"></i></button>
                                 <button class="btn btn-warning btn-circle btn-lg" type="button"><i class="fa fa-times"></i></button>
                                 <button class="btn btn-danger btn-circle btn-lg" type="button"><i class="fa  fa-paperclip"></i></button>
                              </div>
                              <h2>Extra Large</h2>
                              <div class="bs-example">
                                 <button class="btn btn-default btn-circle btn-xl" type="button"><i class="fa fa-check"></i></button>
                                 <button class="btn btn-primary btn-circle btn-xl" type="button"><i class="fa fa-plus"></i></button>
                                 <button class="btn btn-success btn-circle btn-xl" type="button"><i class="fa fa-link"></i></button>
                                 <button class="btn btn-info btn-circle btn-xl" type="button"><i class="fa fa-pencil-square-o"></i></button>
                                 <button class="btn btn-warning btn-circle btn-xl" type="button"><i class="fa fa-times"></i></button>
                                 <button class="btn btn-danger btn-circle btn-xl" type="button"><i class="fa  fa-paperclip"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #NAVS -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Navs</h1>
                     </div>
                     <div class="bs-example-component">
                        <div class="row">
                           <div class="col-lg-4">
                              <h2 class="pgs-nav-tabs">Tabs</h2>
                              <div class="bs-example">
                                 <ul class="nav nav-tabs">
                                    <li class=""><a href="#home" data-toggle="tab">Home</a></li>
                                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                                    <li class="dropdown">
                                       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                       Dropdown <span class="caret"></span>
                                       </a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#dropdown1" data-toggle="tab">Action</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#dropdown2" data-toggle="tab">Another action</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                                 <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade" id="home">
                                       <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                                    </div>
                                    <div class="tab-pane fade active in" id="profile">
                                       <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
                                    </div>
                                    <div class="tab-pane fade" id="dropdown1">
                                       <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork.</p>
                                    </div>
                                    <div class="tab-pane fade" id="dropdown2">
                                       <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater.</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2 id="nav-pills">Accordion</h2>
                              <div class="bs-example">
                                 <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                       <div class="panel-heading">
                                          <h4 class="panel-title">
                                             <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                             Collapsible Group Item #1
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseOne" class="panel-collapse collapse in">
                                          <div class="panel-body">
                                             Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. 
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading">
                                          <h4 class="panel-title">
                                             <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                             Collapsible Group Item #2
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseTwo" class="panel-collapse collapse">
                                          <div class="panel-body">
                                             Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. 
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading">
                                          <h4 class="panel-title">
                                             <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                             Collapsible Group Item #3
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseThree" class="panel-collapse collapse">
                                          <div class="panel-body">
                                             Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2 id="nav-pills">Pills</h2>
                              <div class="bs-example">
                                 <ul class="nav nav-pills">
                                    <li class="active"><a href="#">Home</a></li>
                                    <li><a href="#">Profile</a></li>
                                    <li class="disabled"><a href="#">Disabled</a></li>
                                    <li class="dropdown">
                                       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                       Dropdown <span class="caret"></span>
                                       </a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                              <br>
                              <div class="bs-example">
                                 <ul class="nav nav-pills nav-stacked" style="max-width: 300px;">
                                    <li class="active"><a href="#">Home</a></li>
                                    <li><a href="#">Profile</a></li>
                                    <li class="disabled"><a href="#">Disabled</a></li>
                                    <li class="dropdown">
                                       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                       Dropdown <span class="caret"></span>
                                       </a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Action</a></li>
                                          <li><a href="#">Another action</a></li>
                                          <li><a href="#">Something else here</a></li>
                                          <li class="divider"></li>
                                          <li><a href="#">Separated link</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <h2 id="nav-breadcrumbs">Breadcrumbs</h2>
                              <div class="bs-example">
                                 <ul class="breadcrumb">
                                    <li class="active">Home</li>
                                 </ul>
                                 <ul class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">Library</li>
                                 </ul>
                                 <ul class="breadcrumb" style="margin-bottom: 5px;">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Library</a></li>
                                    <li class="active">Data</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2 id="pagination">Pagination</h2>
                              <div class="bs-example">
                                 <ul class="pagination">
                                    <li class="disabled"><a href="#">«</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">»</a></li>
                                 </ul>
                                 <ul class="pagination pagination-lg">
                                    <li class="disabled"><a href="#">«</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">»</a></li>
                                 </ul>
                                 <ul class="pagination pagination-sm">
                                    <li class="disabled"><a href="#">«</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">»</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2 id="pager">Pager</h2>
                              <div class="bs-example">
                                 <ul class="pager">
                                    <li><a href="#">Previous</a></li>
                                    <li><a href="#">Next</a></li>
                                 </ul>
                                 <ul class="pager">
                                    <li class="previous disabled"><a href="#">← Older</a></li>
                                    <li class="next"><a href="#">Newer →</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-4">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #INDICATORS -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Indicators</h1>
                     </div>
                     <div class="bs-docs-section">
                        <div class="row">
                           <div class="col-lg-12">
                              <h2>Alerts</h2>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-bg-white alert-primary">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Oh snap!</strong> <a class="alert-link" href="#">Change a few things up</a> and try submitting again.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-bg-white alert-success">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Oh snap!</strong> <a class="alert-link" href="#">Change a few things up</a> and try submitting again.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-bg-white alert-info">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Warning!</strong> <a class="alert-link" href="#">Change a few things up</a> and try submitting again.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-bg-white alert-warning">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Well done!</strong> You successfully read <a class="alert-link" href="#">this important alert message</a>.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-bg-white alert-danger">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <div class="icon"><i class="fa fa-check"></i></div>
                                    <strong>Heads up!</strong> This <a class="alert-link" href="#">alert needs your attention</a>, but it's not super important.
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-primary">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="fa fa-check icon-sign"></i>
                                    <strong>Oh snap!</strong> <a class="alert-link" href="#">Change a few things up</a> and try submitting again.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-success">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="fa fa-check icon-sign"></i>
                                    <strong>Warning!</strong> <a class="alert-link" href="#">Change a few things up</a> and try submitting again.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-info">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="fa fa-check icon-sign"></i>
                                    <strong>Well done!</strong> You successfully read <a class="alert-link" href="#">this important alert message</a>.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-warning">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="fa fa-check icon-sign"></i>
                                    <strong>Heads up!</strong> This <a class="alert-link" href="#">alert needs your attention</a>, but it's not super important.
                                 </div>
                              </div>
                              <div class="bs-example">
                                 <div class="alert alert-dismissable alert-danger">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <i class="fa fa-check icon-sign"></i>
                                    <strong>Heads up!</strong> This <a class="alert-link" href="#">alert needs your attention</a>, but it's not super important.
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6">
                              <h2>Labels</h2>
                              <div style="margin-bottom: 40px;" class="bs-example">
                                 <span class="label label-default">Default</span>
                                 <span class="label label-primary">Primary</span>
                                 <span class="label label-success">Success</span>
                                 <span class="label label-warning">Warning</span>
                                 <span class="label label-danger">Danger</span>
                                 <span class="label label-info">Info</span>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <h2>Badges</h2>
                              <div class="bs-example">
                                 <ul class="nav nav-pills">
                                    <li class="active"><a href="#">Home <span class="badge">42</span></a></li>
                                    <li><a href="#">Profile <span class="badge"></span></a></li>
                                    <li><a href="#">Messages <span class="badge">3</span></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #TABLES -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1 id="tables">Tables</h1>
                     </div>
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="bs-example">
                              <div class="panel panel-default">
                                 <div class="panel-body">
                                    <table class="table table-striped table-hover ">
                                       <thead>
                                          <tr>
                                             <th>#</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>1</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>2</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr class="info">
                                             <td>3</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr class="success">
                                             <td>4</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr class="danger">
                                             <td>5</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr class="warning">
                                             <td>6</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr class="active">
                                             <td>7</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <!-- /example -->
                        </div>
                        <div class="col-lg-6">
                           <div class="bs-example">
                              <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4>Table Striped</h4>
                                 </div>
                                 <div class="panel-body">
                                    <table class="table table-striped">
                                       <thead>
                                          <tr>
                                             <th>#</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>1</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>2</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>3</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>4</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>5</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>6</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>7</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <!-- /example -->
                        </div>
                        <div class="col-lg-6">
                           <div class="bs-example">
                              <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4>Table Bordered</h4>
                                 </div>
                                 <div class="panel-body">
                                    <table class="table table-bordered">
                                       <thead>
                                          <tr>
                                             <th>#</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                             <th>Column heading</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>1</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>2</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>3</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>4</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>5</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>6</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                          <tr>
                                             <td>7</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                             <td>Column content</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <!-- /example -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #PROGRESS BAR -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Progress bars</h1>
                     </div>
                     <div class="bs-example">
                        <div class="row">
                           <div class="col-md-12">
                              <h2 class="pgs-nav-tabs">Basic</h2>
                              <div class="progress">
                                 <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar"><span class="sr-only">60% Complete</span></div>
                              </div>
                              <div class="progress">
                                 <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success"><span class="sr-only">40% Complete (success)</span></div>
                              </div>
                              <div class="progress">
                                 <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info"><span class="sr-only">20% Complete</span></div>
                              </div>
                              <div class="progress">
                                 <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning"><span class="sr-only">60% Complete (warning)</span></div>
                              </div>
                              <div class="progress">
                                 <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger"><span class="sr-only">80% Complete (danger)</span></div>
                              </div>
                              <div class="progress">
                                 <div style="width: 35%" class="progress-bar progress-bar-success"><span class="sr-only">35% Complete (success)</span></div>
                                 <div style="width: 20%" class="progress-bar progress-bar-warning"><span class="sr-only">20% Complete (warning)</span></div>
                                 <div style="width: 10%" class="progress-bar progress-bar-danger"><span class="sr-only">10% Complete (danger)</span></div>
                              </div>
                              <h2 class="pgs-nav-tabs">Striped</h2>
                              <div class="progress progress-striped">
                                 <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar"><span class="sr-only">60% Complete</span></div>
                              </div>
                              <div class="progress progress-striped">
                                 <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success"><span class="sr-only">40% Complete (success)</span></div>
                              </div>
                              <div class="progress progress-striped">
                                 <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info"><span class="sr-only">20% Complete</span></div>
                              </div>
                              <div class="progress progress-striped">
                                 <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning"><span class="sr-only">60% Complete (warning)</span></div>
                              </div>
                              <div class="progress progress-striped">
                                 <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger"><span class="sr-only">80% Complete (danger)</span></div>
                              </div>
                              <div class="progress progress-striped">
                                 <div style="width: 35%" class="progress-bar progress-bar-success"><span class="sr-only">35% Complete (success)</span></div>
                                 <div style="width: 20%" class="progress-bar progress-bar-warning"><span class="sr-only">20% Complete (warning)</span></div>
                                 <div style="width: 10%" class="progress-bar progress-bar-danger"><span class="sr-only">10% Complete (danger)</span></div>
                              </div>
                              <h2 class="pgs-nav-tabs">Animated</h2>
                              <div class="progress progress-striped active">
                                 <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar"><span class="sr-only">60% Complete</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #CONTAINERS -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Containers</h1>
                     </div>
                     <div class="bs-docs-section">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="bs-example">
                                 <div class="jumbotron">
                                    <h1>Jumbotron</h1>
                                    <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                                    <p><a class="btn btn-primary btn-lg">Learn more</a></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12">
                              <h2>List groups</h2>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <ul class="list-group">
                                    <li class="list-group-item">
                                       <span class="badge">14</span>
                                       Cras justo odio
                                    </li>
                                    <li class="list-group-item">
                                       <span class="badge">2</span>
                                       Dapibus ac facilisis in
                                    </li>
                                    <li class="list-group-item">
                                       <span class="badge">1</span>
                                       Morbi leo risus
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="list-group">
                                    <a class="list-group-item active" href="#">
                                    Cras justo odio
                                    </a>
                                    <a class="list-group-item" href="#">Dapibus ac facilisis in
                                    </a>
                                    <a class="list-group-item" href="#">Morbi leo risus
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="list-group">
                                    <a class="list-group-item" href="#">
                                       <h4 class="list-group-item-heading">List group item heading</h4>
                                       <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                                    </a>
                                    <a class="list-group-item" href="#">
                                       <h4 class="list-group-item-heading">List group item heading</h4>
                                       <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12">
                              <h2>Panels</h2>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="panel panel-default">
                                    <div class="panel-body">
                                       Basic panel
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading">Panel heading</div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                    <div class="panel-footer">Panel footer</div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">Panel primary</h3>
                                    </div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                                 <div class="panel panel-success">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">Panel success</h3>
                                    </div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                                 <div class="panel panel-warning">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">Panel warning</h3>
                                    </div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="panel panel-danger">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">Panel danger</h3>
                                    </div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                                 <div class="panel panel-info">
                                    <div class="panel-heading">
                                       <h3 class="panel-title">Panel info</h3>
                                    </div>
                                    <div class="panel-body">
                                       Panel content
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12">
                              <h2>Wells</h2>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="well">
                                    Look, I'm in a well!
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="well well-sm">
                                    Look, I'm in a small well!
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4">
                              <div class="bs-example">
                                 <div class="well well-lg">
                                    Look, I'm in a large well!
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
            <!-- #GALLERY ITEMS -->	
            <div class="bs-example-navbar clearfix">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-header">
                        <h1>Gallery Items</h1>
                     </div>
                     <div class="bs-docs-section">
                        <div class="row">
                           <div class="col-lg-6 col-sm-12">
                              <h2>Thumbnails</h2>
                              <div class="row">
                                 <div class="col-sm-6">
                                    <a class="thumbnail" href="#">
                                    <img src="assets/images/img1.png" alt="Image">
                                    </a>
                                 </div>
                                 <div class="col-sm-6">
                                    <a class="thumbnail" href="#">
                                    <img src="assets/images/img2.png" alt="Image">
                                    </a>
                                 </div>
                                 <div class="col-sm-6">
                                    <a class="thumbnail" href="#">
                                    <img src="assets/images/img3.png" alt="Image">
                                    </a>
                                 </div>
                                 <div class="col-sm-6">
                                    <a class="thumbnail" href="#">
                                    <img src="assets/images/img4.png" alt="Image">
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6 col-sm-12">
                              <h2>Thumbnails with Captions</h2>
                              <div class="col-sm-6">
                                 <div class="thumbnail">
                                    <img src="assets/images/img3.png"  alt="Image">
                                    <div class="caption">
                                       <h3>Thumbnail Label</h3>
                                       <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                       <button class="btn btn-warning">Read More <i class="fa fa-arrow-right"></i></button>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="thumbnail">
                                    <img src="assets/images/img3.png" alt="Image">
                                    <div class="caption">
                                       <h3>Thumbnail Label</h3>
                                       <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                       <button class="btn btn-warning">Read More <i class="fa fa-arrow-right"></i></button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12">
                              <div class="col-lg-6 col-lg-offset-3">
                                 <h2>Carousal</h2>
                                 <div data-ride="carousel" class="carousel slide" id="carousel-example-generic">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                       <li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                                       <li data-slide-to="1" data-target="#carousel-example-generic" class=""></li>
                                       <li data-slide-to="2" data-target="#carousel-example-generic" class=""></li>
                                    </ol>
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                       <div class="item active">
                                          <img src="assets/images/img1.png" alt="Image 1">
                                          <div class="carousel-caption">
                                             Easily add captions for your image
                                          </div>
                                       </div>
                                       <div class="item">
                                          <img src="assets/images/img2.png"  alt="Image 2">
                                          <div class="carousel-caption">
                                             Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vehicula ut id elit.
                                          </div>
                                       </div>
                                       <div class="item">
                                          <img src="assets/images/img3.png"  alt="Image 1">
                                          <div class="carousel-caption">
                                             Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vehicula ut id elit.
                                          </div>
                                       </div>
                                       <div class="item">
                                          <img src="assets/images/img4.png"  alt="Image 1">
                                       </div>
                                    </div>
                                    <!-- Controls -->
                                    <a data-slide="prev" role="button" href="#carousel-example-generic" class="left carousel-control">
                                    <span class="fa fa-chevron-left"></span>
                                    </a>
                                    <a data-slide="next" role="button" href="#carousel-example-generic" class="right carousel-control">
                                    <span class="fa fa-chevron-right"></span>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="source-modal" class="modal fade">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                  <h4 class="modal-title">Source Code</h4>
               </div>
               <div class="modal-body">
                  <pre></pre>
               </div>
            </div>
         </div>
      </div>
	  
	  <!-- #JS LINKS -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="assets/plugins/iCheck-master/js/icheck.js?v=1.0.2"></script>
      <script>
         $(document).ready(function(){
           $('input').iCheck({
         	checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
           });
         });
      </script>
      <script src="assets/plugins/iCheck-master/js/icheck.js?v=1.0.2"></script>
      
	  <!-- Not Needed for your theme just for code popup display -->
      <script type="text/javascript">
         (function(){
         
         var $button = $("<div id='source-button' class='btn btn-primary btn-xs'>&lt; &gt;</div>").click(function(){
           var html = $(this).parent().html();
           html = cleanSource(html);
           $("#source-modal pre").text(html);
           $("#source-modal").modal();
         });
         
         $('.bs-example [data-toggle="popover"]').popover();
         $('.bs-example [data-toggle="tooltip"]').tooltip();
         
         $(".bs-example").hover(function(){
           $(this).append($button);
           $button.show();
         }, function(){
           $button.hide();
         });
         
         function cleanSource(html) {
           var lines = html.split(/\n/);
         
           lines.shift();
           lines.splice(-1, 1);
         
           var indentSize = lines[0].length - lines[0].trim().length,
               re = new RegExp(" {" + indentSize + "}");
         
           lines = lines.map(function(line){
             if (line.match(re)) {
               line = line.substring(indentSize);
             }
         
             return line;
           });
         
           lines = lines.join("\n");
         
           return lines;
         }
         
         })();
         
      </script>
   </body>
</html>
